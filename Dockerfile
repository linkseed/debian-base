FROM debian:buster

RUN apt-get update -y && apt-get upgrade -y && \
    apt-get install -y curl git locales sudo tzdata
# Cleanup
RUN apt-get autoremove -y && apt-get clean -y && \
    rm -rf /var/lib/apt/lists/*

ENV USER=worker \
    HOME=/home/work \
    LANG=en_US.UTF-8 \
    UID=10001 \
    GID=10001

RUN echo "LANG=${LANG}" > /etc/default/locale && \
    localedef -i $(echo ${LANG} | sed 's/\..*//') -c \
              -f $(echo ${LANG} | sed 's/.*\.//') ${LANG} && \
    ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime && \
    groupadd -g ${GID} ${USER} && \
    useradd -u ${UID} -g ${GID} -s /bin/bash -G sudo -d ${HOME} -m -N ${USER} && \
    mkdir -p ${HOME}/.config ${HOME}/.ssh ${HOME}/tmp && \
    chown -R ${UID}:${GID} ${HOME} &&\
    echo "${USER} ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/${USER} && \
    chmod 0440 /etc/sudoers.d/${USER}

USER ${USER}
WORKDIR ${HOME}
CMD ["bash", "-l"]
